const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const data = require("./data");

const PORT = process.env.PORT | 3000;

const app = express();
const server = http.createServer(app);

const ws = new WebSocket.Server({ server });

function random(min, max){
    return Math.floor(Math.random()*max+min);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

ws.on("connection", async (ws) => {
    for(let d of data){
        ws.send(JSON.stringify(d), (err)=>{
            if(err){
                console.error(err);
            }
        });
        await sleep(2000);
    }
    ws.close();
});

server.listen(PORT, () => {
    console.log(`Running server at port ${PORT}`);
});
